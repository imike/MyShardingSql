package transaction;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.TransactionIsolationLevel;

import business.interfaces.RoleMapper;
import com.sql.mysql.sharding.runnable.ShardingTransactionRunnable;

import business.bean.Role;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShardingWithTransactionAdvanced {

	public static void main(String[] args) throws Exception {
		// 初始化时，请设定你需要的参数，比如:Executor类型,是否自动提交,事务级别
		log.debug("begin to execute ");
		Role result = new ShardingTransactionRunnable<Role>(TransactionIsolationLevel.READ_COMMITTED) {
			@Override
			public Role execute(SqlSession sqlSession) {
				// 从这里,开始写任何你需要的的业务代码
				log.debug("{}", sqlSession);
				// 处于一个事务里，事务相关的东西，业务不需要关心,框架已经做好了
				RoleMapper userMapper = sqlSession.getMapper(RoleMapper.class);// 获得mapper
				log.debug("{}", userMapper);
				// delete
				Role role = new Role();
				role.setId(13);
				userMapper.deleteRole(role);
				// insert
				role.setId(13);
				role.setTitle("xxx");
				role.setAuthor("yyy");
				userMapper.insertRole(role);
				// select
				role = userMapper.getRole0(11, 111);
				role = userMapper.getRole0(11, 1111);
				role = userMapper.getRole0(11, 2222);
				return role;
			}
		}.run();
		//
		log.debug("" + result);
	}
}
