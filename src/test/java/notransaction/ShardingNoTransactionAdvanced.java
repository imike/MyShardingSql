package notransaction;

import com.sql.mysql.sharding.mapper.ShardingMapperUtils;

import business.bean.Role;
import business.interfaces.RoleMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShardingNoTransactionAdvanced {

	public static void main(String[] args) {
		//
		//
		RoleMapper mapper = ShardingMapperUtils.getMapper(RoleMapper.class);
		log.info("mapper {}", mapper);
		Role role = new Role();
		role.setId(100);
		role.setAuthor("xxx");
		role.setTitle("yyy");
		mapper.deleteRole(role);
		log.info("删除主键100成功");
		log.info("start");
		mapper = ShardingMapperUtils.getMapper(RoleMapper.class);
		role = mapper.getRole0(13, 2);

		//
		mapper = ShardingMapperUtils.getMapper(RoleMapper.class);
		role = new Role();
		role.setId(10000);
		role.setAuthor("xxx");
		role.setTitle("yyy");
		mapper.insertRole(role);
		//
		// try {
		// Thread.currentThread().sleep(6*1000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		//
		mapper = ShardingMapperUtils.getMapper(RoleMapper.class);
		role = mapper.getRole0(13, 2);
		//
		//
		mapper = ShardingMapperUtils.getMapper(RoleMapper.class);
		role = new Role();
		role.setAuthor("xxx");
		role.setTitle("yyy");
		mapper.insertRole(role);

	}
}
