package obsolete;

import org.apache.ibatis.session.SqlSession;

import com.sql.mysql.sharding.session.ShardingSqlSessionFactory;

import business.bean.Role;
import business.interfaces.RoleMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShardingNoTransaction {
	static {
		try {
			Class.forName("com.sql.mysql.sharding.helper.SqlSessionFactoryHelper");
			log.info("所有数据源 初始化完毕...");
		} catch (ClassNotFoundException e) {
			log.error("error {}", e);
			System.exit(-1);
		}
	}

	public static void main(String[] args) {
		SqlSession sqlSession = null;
		try {
			// 借助于辅助类获得session
			// 不开启事务,自动提交
			// delete
			sqlSession = ShardingSqlSessionFactory.getSqlSessionFactory().openSession(true);
			RoleMapper userMapper = sqlSession.getMapper(RoleMapper.class);
			Role role = new Role();
			role.setId(13);
			userMapper.deleteRole(role);
			sqlSession.close();
			//
			//
			// insert

			sqlSession = ShardingSqlSessionFactory.getSqlSessionFactory().openSession(true);
			userMapper = sqlSession.getMapper(RoleMapper.class);
			role = new Role();
			role.setId(13);
			role.setTitle("xxxxxxx");
			role.setAuthor("yyyyyyy");
			userMapper.insertRole(role);
			sqlSession.close();

			//
			//
			// Select

			sqlSession = ShardingSqlSessionFactory.getSqlSessionFactory().openSession(true);
			userMapper = sqlSession.getMapper(RoleMapper.class);
			role = userMapper.getRole0(13,2);
			log.info("role ------> " + role);
			sqlSession.close();

			// sqlSession.commit();自动提交不需要这个
		} catch (Exception e) {
			// sqlSession.rollback();自动提交不需要这个
			log.error(e.toString());
		}
	}
}
