package obsolete;

import org.apache.ibatis.session.SqlSession;

import com.sql.mysql.sharding.session.ShardingSqlSessionFactory;

import business.bean.Role;
import business.interfaces.RoleMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShardingWithTransaction {

	public static void main(String[] args) {
		// 可以考虑一个Runnable来封装
		SqlSession sqlSession = null;
		try {
			// 借助于辅助类获得session
			// 开启事务
			sqlSession = ShardingSqlSessionFactory.getSqlSessionFactory().openSession(false);
			{
				// 从这里,开始写你的业务代码
				// 获得mapper
				// LOGGER.info("---------------------------------------------------");
				RoleMapper userMapper = sqlSession.getMapper(RoleMapper.class);
				log.info("---------------------------------------------------");
				// delete
				Role role = new Role();
				role.setId(13);
				userMapper.deleteRole(role);
				log.info("---------------------------------------------------");
				// insert
				role.setId(13);
				role.setTitle("xxxxxxx");
				role.setAuthor("yyyyyyy");
				userMapper.insertRole(role);
				log.info("---------------------------------------------------");
				// Select
				role = userMapper.getRole0(13, 11);
				log.info("---------------------------------------------------");
				log.info("userMapper--->" + userMapper);
				role = userMapper.getRole0(13, 21);
				log.info("role ---> " + role);
				log.info("---------------------------------------------------");
			}
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			log.equals(e.toString());
		} finally {
			if (null != sqlSession) {
				sqlSession.close();
			}
		}
		// end
	}
}
