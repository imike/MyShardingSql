#! /bin/sh
#获取当前目录
cd `dirname $0`
PWD=`pwd`


CLASSPATH="";
#当前包打入CLASSPATH
for item in ./*.jar;
do CLASSPATH=$PWD/$item:"$CLASSPATH";
done

#依赖包打入CLASSPATH
for item in lib/*.jar;
do CLASSPATH=$PWD/$item:"$CLASSPATH";
done

echo "CLASSPATH="$CLASSPATH


nohup java -server  -Xmx1024m -Xms1024m -Xmn512m -classpath $PWD/conf:$CLASSPATH: logback_debug.Test  &