package business.interfaces;

import com.sql.mysql.sharding.annotation.SqlField;

import org.apache.ibatis.annotations.Param;

import com.sql.mysql.sharding.annotation.ParameterIndex;

import business.bean.Role;

//关于注解ShardingKey
//1)方法上的注解--->对应到xml里的sql的占位符的值,字段要写sql里的占位符
//2)方法的字段上的注解--->直接取值
//系统启动时,会扫描所有包含@ShardingKeyByParameterIndex或者@ShardingKeyBySqlField的接口类
public interface RoleMapper {
	public Role getRole(int id);

	@ParameterIndex(value = 0)
	public Role getRole0(@Param(value = "id") int id, int aaa);

	@SqlField(value = "id")
	public void insertRole(Role r);

	public void deleteRole(String id);

	@SqlField(value = "id") // 用id来做ShardingKey
	public void deleteRole(Role r);

}
