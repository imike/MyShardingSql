package com.sql.mysql.sharding.runnable;

public abstract class BaseRunnable<T> {
	public abstract T run() throws Exception;
}
