package com.sql.mysql.sharding.utils;

public class NumberUtils {
	public static long Object2Long(Object obj) throws Exception {
		if (obj instanceof Long) {
			return (long) obj;
		} else if (obj instanceof Integer) {
			return ((Integer) obj).intValue();
		} else {
			throw new Exception("sharding key must be int or long");
		}
	}
}
