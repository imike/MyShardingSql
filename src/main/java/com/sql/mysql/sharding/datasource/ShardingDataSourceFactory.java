package com.sql.mysql.sharding.datasource;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.datasource.DataSourceFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShardingDataSourceFactory implements DataSourceFactory {

	protected Properties properties;

	@Override
	public void setProperties(Properties props) {
		this.properties = props;
	}

	@Override
	public DataSource getDataSource() {
		try {
			return new ShardingDataSource(properties);
		} catch (RuntimeException e) {
			log.error("exception {}", e);
			throw e;
		} catch (Exception e) {
			log.error("exception {}", e);
			throw new RuntimeException("init data source error", e);
		}
	}

}
