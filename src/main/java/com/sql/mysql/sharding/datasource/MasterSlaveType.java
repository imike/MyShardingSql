package com.sql.mysql.sharding.datasource;

public enum MasterSlaveType {
	MASTER, SLAVE
}
