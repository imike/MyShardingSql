package com.sql.mysql.sharding.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.alibaba.fastjson.JSON;
import com.sql.mysql.sharding.config.Range;
import com.sql.mysql.sharding.config.Shard;
import com.sql.mysql.sharding.config.ShardGroup;
import com.sql.mysql.sharding.config.Table;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShardingConfigCenter {

	public static String RESERVED_GLOBAL_REMOTE_CONFIG_KEY = "RESERVED_GLOBAL_REMOTE_CONFIG_KEY";
	public static long W = 10000;

	// 加上synchronzied,保证顺序执行
	public static Properties getAllConfig() {
		Properties prop = new Properties();
		// 获取配置中心的地址,比如从run.sh中的-D参数获取
		// 然后获取响应,是一个JSON体
		// 再解析生成Properties
		prop.setProperty("username", "root");
		prop.setProperty("password", ".");
		prop.setProperty("defaultCatalog", "ambari");
		prop.setProperty("initialSize", "1");
		prop.setProperty("minIdle", "6");
		prop.setProperty("maxActive", "30");
		// 反序列化ShardGroup
		List<ShardGroup> shardGroups = new ArrayList<ShardGroup>();
		// 添加第1个group
		{
			ShardGroup shardGroup = new ShardGroup();
			shardGroups.add(shardGroup);
			List<Range> ranges = new ArrayList<Range>();
			ranges.add(new Range("0", "" + 4000 * W));
			ranges.add(new Range("" + 10000 * W, "" + 12000 * W));
			shardGroup.setRanges(ranges);
			shardGroup.setParam("2");
			//// 添加ShardList
			List<Shard> shards = new ArrayList<Shard>();
			shardGroup.setShards(shards);
			{
				{
					// 构造Shard0
					Shard shard = new Shard();
					shard.setMachines(new String[] { "127.0.0.1:3306" });
					shards.add(shard);
					shard.setName("Shard0");
					Set<String> set = new HashSet<String>();
					set.add("0");
					shard.setValues(set);
					// 增加自己的Table
					List<Table> tables = new ArrayList<Table>();
					shard.setTables(tables);

					// 增加Table0
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						tmpList.add(new Range("0", "" + 2000 * W));
						tmpList.add(new Range("" + 10000 * W, "" + 11000 * W));
						table.setRanges(tmpList);
						table.setSuffix("_0");

					}
					// 增加Table1
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						tmpList.add(new Range("" + 2000 * W, "" + 4000 * W));
						tmpList.add(new Range("" + 11000 * W, "" + 12000 * W));
						table.setRanges(tmpList);
						table.setSuffix("_1");
					}
				}
				{
					// 构造Shard1
					Shard shard = new Shard();
					shard.setMachines(new String[] { "127.0.0.1:3306" });
					shards.add(shard);
					shard.setName("Shard1");
					Set<String> set = new HashSet<String>();
					set.add("1");
					shard.setValues(set);
					// 增加自己的Table
					List<Table> tables = new ArrayList<Table>();
					shard.setTables(tables);

					// 增加Table0
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						tmpList.add(new Range("" + 0, "" + 2000 * W));
						tmpList.add(new Range("" + 10000 * W, "" + 11000 * W));
						table.setRanges(tmpList);
						table.setSuffix("_0");

					}
					// 增加Table1
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						tmpList.add(new Range("" + 2000 * W, "" + 4000 * W));
						tmpList.add(new Range("" + 11000 * W, "" + 12000 * W));
						table.setRanges(tmpList);
						table.setSuffix("_1");
					}
				}
			}
			shardGroup.setName("ShardGroup0");
		}

		// 添加第2个group
		{
			ShardGroup shardGroup = new ShardGroup();
			shardGroups.add(shardGroup);
			List<Range> ranges = new ArrayList<Range>();
			ranges.add(new Range("" + 4000 * W, "" + 10000 * W));
			shardGroup.setRanges(ranges);
			shardGroup.setParam("6");
			//// 添加ShardList
			List<Shard> shards = new ArrayList<Shard>();
			shardGroup.setShards(shards);
			{
				// 构造Shard2
				Shard shard = new Shard();
				shard.setMachines(new String[] { "127.0.0.1:3306" });
				shards.add(shard);
				shard.setName("Shard2");
				Set<String> set = new HashSet<String>();
				set.add("0");
				shard.setValues(set);
				// 增加自己的Table
				List<Table> tables = new ArrayList<Table>();
				shard.setTables(tables);
				{
					// 增加Table0
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						Range range = new Range();
						range.setBegin("" + 4000 * W);
						range.setEnd("" + 10000 * W);
						tmpList.add(range);
						table.setRanges(tmpList);
						table.setSuffix("_0");
					}

				}
			}
			{
				// 构造Shard3
				Shard shard = new Shard();
				shard.setMachines(new String[] { "127.0.0.1:3306" });
				shards.add(shard);
				shard.setName("Shard3");
				Set<String> set = new HashSet<String>();
				set.add("1");
				set.add("2");
				shard.setValues(set);
				// 增加自己的Table
				List<Table> tables = new ArrayList<Table>();
				shard.setTables(tables);
				{
					// 增加Table_0
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						Range range = new Range();
						range.setBegin("" + 4000 * W);
						range.setEnd("" + 7000 * W);
						tmpList.add(range);
						table.setRanges(tmpList);
						table.setSuffix("_0");
					}
					// 增加Table_1
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						Range range = new Range();
						range.setBegin("" + 7000 * W);
						range.setEnd("" + 10000 * W);
						tmpList.add(range);
						table.setRanges(tmpList);
						table.setSuffix("_1");
					}
				}
			}
			{
				// 构造Shard4
				Shard shard = new Shard();
				shard.setMachines(new String[] { "127.0.0.1:3306" });
				shards.add(shard);
				shard.setName("Shard4");
				Set<String> set = new HashSet<String>();
				set.add("3");
				set.add("4");
				set.add("5");
				shard.setValues(set);
				// 增加自己的Table
				List<Table> tables = new ArrayList<Table>();
				shard.setTables(tables);
				{
					// 增加Table_0
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						Range range = new Range();
						range.setBegin("" + 4000 * W);
						range.setEnd("" + 6000 * W);
						tmpList.add(range);
						table.setRanges(tmpList);
						table.setSuffix("_0");
					}
					// 增加Table_1
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						Range range = new Range();
						range.setBegin("" + 6000 * W);
						range.setEnd("" + 8000 * W);
						tmpList.add(range);
						table.setRanges(tmpList);
						table.setSuffix("_1");
					}
					// 增加Table_2
					{
						Table table = new Table();
						tables.add(table);
						List<Range> tmpList = new ArrayList<Range>();
						Range range = new Range();
						range.setBegin("" + 8000 * W);
						range.setEnd("" + 10000 * W);
						tmpList.add(range);
						table.setRanges(tmpList);
						table.setSuffix("_2");
					}
				}
			}
			shardGroup.setName("ShardGroup1");
		}
		prop.put(RESERVED_GLOBAL_REMOTE_CONFIG_KEY, JSON.toJSONString(shardGroups));
		log.info("succeed to get url remote");
		return prop;
	}

}
