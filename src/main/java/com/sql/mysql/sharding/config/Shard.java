package com.sql.mysql.sharding.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import lombok.Data;

@Data
public class Shard {// 映射到物理机器
	@SuppressWarnings("rawtypes")
	private Set values;// 会有很多个,根据不同的算法,落到了这里就行
	// 数据库名字全局统一,不涉及重写
	private List<Table> tables = new ArrayList<Table>();// 表的定义
	private String name;
	private String[] machines;
	private DataSource dataSource;// 是一个MasterSlaveDataSource

}
