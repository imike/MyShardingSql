package com.sql.mysql.sharding.config;

import java.util.List;

import lombok.Data;

@Data
public class Table {
	private List<Range> ranges;
	private String suffix;// 比如_0,_1,改写sql时候需要的
}
