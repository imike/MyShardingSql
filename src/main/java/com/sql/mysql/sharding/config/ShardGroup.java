package com.sql.mysql.sharding.config;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class ShardGroup {
	private ShardGroupType type = ShardGroupType.MOD;// 类型
	private List<Range> ranges; // 比如: 0-4000W&10000W-12000W,包含低位不包含高位
	private String param;// 根据type的不同而赋予不同的具体的算法含义
	private List<Shard> shards = new ArrayList<Shard>();// 包含多个Shard,全局唯一
	private String name;

	public DataSource getDataSourceByShardName(String shardName) {
		// 1)判断参数
		if (null == shardName || null == shards) {
			log.info("shardName | shards is null");
			return null;
		}
		// 遍历查找
		for (Shard shard : shards) {
			if (shard.getName().trim().equals(shardName.trim())) {
				return shard.getDataSource();
			}
		}
		// 没找到,返回null
		return null;
	}
}
