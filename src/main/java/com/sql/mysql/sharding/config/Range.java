package com.sql.mysql.sharding.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class Range {
	private String begin;
	private String end;

	@Override
	public String toString() {
		return "[" + begin + "," + end + "]";
	}

	public boolean in(long key) {
		long beginLongValue = Long.parseLong(begin);
		long endLongValue = Long.parseLong(end);
		if (beginLongValue <= key && key < endLongValue) {
			return true;
		} else {
			return false;
		}
	}

	public Range() {
		// 被坑惨,原来fastjson必须要有1个空的构造函数
	}

	public Range(String b, String e) {
		begin = b;
		end = e;
	}

	// public String getBegin() {
	// return begin;
	// }
	//
	// public void setBegin(String begin) {
	// this.begin = begin;
	// }

	// public String getEnd() {
	// return end;
	// }
	//
	// public void setEnd(String end) {
	// this.end = end;
	// }

	public static void main(String[] args) {
		log.debug("");
		// Range range = new Range("0", "1000");
		// log.info("{}", range.in(0));
		// log.info("{}", range.in(10));
		// log.info("{}", range.in(1000));
		// log.info("{}", range.in(1001));
	}
}
