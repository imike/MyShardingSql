package com.sql.mysql.sharding.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ParameterIndex {

	int value() default -1;// 用于分片的属性名,分片本身意味着分库分表
	//
	// String service() default "";//
	//
	// String edition() default "";//

}
