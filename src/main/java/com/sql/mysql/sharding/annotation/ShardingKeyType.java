package com.sql.mysql.sharding.annotation;

public enum ShardingKeyType {
	SQL_FIELD, // 方法上的
	PARAMETER_INDEX, // 参数上的
	OTHER// 其它
}
