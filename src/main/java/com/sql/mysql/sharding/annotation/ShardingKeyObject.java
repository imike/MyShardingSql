package com.sql.mysql.sharding.annotation;

import java.lang.reflect.Method;

import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.type.JdbcType;

import lombok.Data;

@Data
public class ShardingKeyObject {
	private Method method;
	private ShardingKeyType annotationType = ShardingKeyType.OTHER;
	private String FiedName;
	private int parameterIndex;
	private Object value = null;// 最终的分片值,如果是1个事务,那就必须一直保持同1个值才可以
	private JdbcType jdbcType;// 最终的jdbcType,不要取parameterMapping里的
	private ParameterMapping parameterMapping;
}
