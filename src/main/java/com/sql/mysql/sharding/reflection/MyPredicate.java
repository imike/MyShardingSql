package com.sql.mysql.sharding.reflection;

import com.google.common.base.Predicate;

@SuppressWarnings("rawtypes")
public class MyPredicate implements Predicate {

	@Override
	public boolean apply(Object input) {
		// log.debug("input {}", input.getClass());
		if (null != input && input instanceof String) {
			String path = ((String) input).trim();
			if (path.endsWith(".class")) {
				return true;
			}
		}
		return false;
	}

}
