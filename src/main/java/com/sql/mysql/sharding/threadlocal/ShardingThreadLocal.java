package com.sql.mysql.sharding.threadlocal;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;

import com.sql.mysql.sharding.annotation.ShardingKeyObject;

public class ShardingThreadLocal {
	public static void reset(boolean resetAll, boolean inTransaction) {
		// reset all,reset everything
		ShardingThreadLocal.AutoCommitThreadLocal.remove();// auto Commit
		ShardingThreadLocal.MappedStatementThreadLocal.remove();// sql command
		ShardingThreadLocal.BoundSqlThreadLocal.remove();// sql
		ShardingThreadLocal.ShardingKeyObjectThreadLocal.remove();// sharding
																	// key
		if (resetAll || false == inTransaction) {
			// 在事务里的一定要保持
			ShardingThreadLocal.LastShardingKeyIDThreadLocal.remove();
		}
	}

	// ShardingKeyValue---保持上一次的分片键的值
	public static ThreadLocal<Long> LastShardingKeyIDThreadLocal = new ThreadLocal<Long>() {
		public Long initialValue() {
			return null;
		}
	};

	// ShardingKeyObject
	public static ThreadLocal<ShardingKeyObject> ShardingKeyObjectThreadLocal = new ThreadLocal<ShardingKeyObject>() {
		public ShardingKeyObject initialValue() {
			return null;
		}
	};

	// sql操作类型
	public static ThreadLocal<MappedStatement> MappedStatementThreadLocal = new ThreadLocal<MappedStatement>() {
		public MappedStatement initialValue() {
			return null;
		}
	};

	// 是否自动提交
	public static ThreadLocal<Boolean> AutoCommitThreadLocal = new ThreadLocal<Boolean>() {
		public Boolean initialValue() {//
			return true;
		}
	};

	// sql语句
	// 如果sql改写失败就会为null!!!
	public static ThreadLocal<BoundSql> BoundSqlThreadLocal = new ThreadLocal<BoundSql>() {
		public BoundSql initialValue() {
			return null;
		}
	};

}
